<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah huruf</title>
</head>
<body>
    <h1>Soal 2 Ubah Huruf</h1>

    <?php
        function ubah_huruf($string){
            $convert = ['a'=>'b', 'b'=>'c', 'c'=>'d', 'd'=>'e', 'e'=>'f', 'f'=>'g', 'g'=>'h', 'h'=>'i',
                        'i'=>'j', 'j'=>'k', 'k'=>'l', 'l'=>'m', 'm'=>'n', 'n'=>'o', 'o'=>'p', 'p'=>'q', 
                        'q'=>'r', 'r'=>'s', 's'=>'t', 't'=>'u', 'u'=>'v', 'v'=>'w', 'w'=>'x', 'x'=>'y', 'y'=>'z', 'z'=>'a',];
            $string     = strtolower($string);
            $array_data = str_split($string);
            $new_data   = '';

            foreach ($array_data as  $value) {
            $new_data .= $convert[$value]."";
        }
        return $new_data;
        }

        // TEST CASES
        echo ubah_huruf('wow'); echo "<br>"; // xpx
        echo ubah_huruf('developer'); echo "<br>";// efwfmpqfs
        echo ubah_huruf('laravel'); echo "<br>";// mbsbwfm
        echo ubah_huruf('keren'); echo "<br>";// lfsfo
        echo ubah_huruf('semangat'); echo "<br>";// tfnbohbu

    ?>

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <footer>
        <h5>copyright &copy; 2021 | @trismanhady</h5>
    </footer>
</body>
<style>
    footer{
    position: static;
    background-color: black;
    height: 30px;
    padding-top: 2px;
    }

footer h5{
    text-align: center;
    color: white;
    margin: 3px;

    }
</style>
</html>