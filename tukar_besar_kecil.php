<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tukar Besar Kecil</title>
</head>
<body>
    <h1>Soal 3 Tukar Besar Kecil</h1>

    <?php

        function tukar_besar_kecil($string){
        //kode di sini
            $convert =  ['a'=>'A', 'b'=>'B', 'c'=>'C', 'd'=>'D', 'e'=>'E', 'f'=>'F', 'g'=>'G', 'h'=>'H',
                        'i'=>'I', 'j'=>'J', 'k'=>'K', 'l'=>'L', 'm'=>'M', 'n'=>'N', 'o'=>'O', 'p'=>'P', 
                        'q'=>'Q', 'r'=>'R', 's'=>'S', 't'=>'T', 'u'=>'U', 'v'=>'V', 'w'=>'W', 'x'=>'X', 'y'=>'Y', 'z'=>'Z',
                        'A'=>'a', 'B'=>'b', 'C'=>'c', 'D'=>'d', 'E'=>'e', 'F'=>'f', 'G'=>'g', 'H'=>'h',
                        'I'=>'i', 'J'=>'j', 'K'=>'k', 'L'=>'l', 'M'=>'m', 'N'=>'n', 'O'=>'o', 'P'=>'p', 
                        'Q'=>'q', 'R'=>'r', 'S'=>'s', 'T'=>'t', 'U'=>'u', 'V'=>'v', 'W'=>'w', 'X'=>'x', 'Y'=>'y', 'Z'=>'z',
                        ' '=> ' ', '1'=> '1', '2'=> '2', '3'=> '3', '4'=> '4', '5'=> '5', '6'=> '6', '7'=> '7', '8'=> '8',
                        '9'=> '9', '10'=> '10', '0'=> '0', '-'=> '-', '!'=> '!',];
            $array_data = str_split($string);
            $new_data   = '';

            foreach ($array_data as  $value) {
            $new_data .= $convert[$value]."";
        }
        return $new_data;
        }

        // TEST CASES
        echo tukar_besar_kecil('Hello World'); echo "<br>";  // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); echo "<br>"; // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); echo "<br>"; // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); echo "<br>"; // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); echo "<br>"; // "001-a-3-5tRDyw"

    ?>

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <footer>
        <h5>copyright &copy; 2021 | @trismanhady</h5>
    </footer>
</body>
<style>
    footer{
    position: static;
    background-color: black;
    height: 30px;
    padding-top: 2px;
    }

footer h5{
    text-align: center;
    color: white;
    margin: 3px;

    }
</style>
</html>