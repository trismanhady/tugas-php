<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tentukan Nilai</title>
</head>
<body>
    <h1>Soal 1 Tentukan Nilai</h1>

    <?php
        //tentukan_nilai = [];
        function tentukan_nilai($number)
        {
            if($number >= 80){
                echo "Sangat Baik <br>";
            } elseif ($number >= 70) {
                echo "Baik <br>";
            } elseif ($number >= 50) {
                echo "Cukup <br>";
            } else {
                echo "Kurang <br>";
            }

        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
        ?>

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <footer>
        <h5>copyright &copy; 2021 | @trismanhady</h5>
    </footer>
</body>
<style>
    footer{
    position: static;
    background-color: black;
    height: 30px;
    padding-top: 2px;
    }

footer h5{
    text-align: center;
    color: white;
    margin: 3px;

    }
</style>
</html>